# DatePickerOH

#### 项目介绍

- 项目名称：日期选择
- 所属系列：鸿蒙OpenHarmony的第三方组件开发
- 功能：实现了以日历的形式根据不同年月显示相应的日期值的效果，点击某日期后在弹出的时间选择框中滑动选择时间，会将最终选择的具体日期时间显示出来。
- 调用差异：重构
- 开发版本：sdk9，DevEco Studio 3.1 Canary1
- 项目作者和维护人：李珂，蔡志杰，曹天恒
- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)

![输入图片说明](datePicker.gif)

  

####  项目介绍

- 编程语言：JavaScript
- 本项目提供了一个以日历形式显示的支持点击选择日期和时间的组件，在选择后会将具体的日期时间显示在相应的控件中，使用时将组件引入对应的hml文件中即可。

#### 使用教程

1.将组件涉及的hml、js、css文件放到src->main->js->MainAbility下新建的component文件夹中。

![输入图片说明](763962f80bb4526b595ac5e9943b339.png)

2. 在相应的hml文件的最开头处，通过<element></element>标签引入DatePicker_ohos组件的源文件。

```html
<element name='datepicker' src='../../common/library/datepicker.hml'></element>
```
3. 在hml文件的相应布局处，添加组件并设置用于接收组件内部传值的处理事件。

```html
<div class="container">
     <datepicker show="{{isShow}}" @get-date="Clicked"></datepicker>
</div>
```
4. 在组件内部的JS文件中通过this.$emit()向Demo部分传值。

```javascript
textonchange(){
        ...
        this.$emit('getDate', {result: this.year_month_day_time});
    },
```
5. 在引用组件的Demo主js文件中接收传来的日期时间值更新相应字段。

```javascript
Clicked (e) {
    this.year_month_day_time = e.detail.result;
},
```

#### 版本迭代

- v0.1.0-alpha

#### 版权和许可信息

- DatePicker_ohos经过[Apache License, version 2.0](https://gitee.com/link?target=http%3A%2F%2Fwww.apache.org%2Flicenses%2FLICENSE-2.0)授权许可。
