/*
 *    Copyright 2016 alongubkin
 *    Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
import router from '@system.router';
import prompt from '@system.prompt';
export default {
    data: {
        title: "",
        month:0,
        year:0,
        day:'',
        allday:'',
        year_month:'',
        dayList:[],
        fistdate:'',
        xinqi:'',
        firstday:0,
        selectedday:0,//选中的具体日期
        index:0,
        timevalue:'default timevalue',
        timeselect:'12:25',
        year_month_day_time:'',
        flags:[],
    },

    data1() {
        return {
            showObj: this.showObject,
        };
    },


    onInit() {
        this.title = this.$t('strings.world');
        var today = new Date();//获取当前时间
        var year=today.getFullYear();//获取当前的年份
        var month=today.getMonth()+1;//获取当前月
        var day=today.getDate();//获取当前月
        var allday=0;
        this.year = year;
        this.month = month;
        this.day = day;
        this.allday = allday;
        this.year_month = year + "年" + month + "月";

        this.count();
        var daylist = [];
        this.dayList = [];
        var month = 10;
        month = this.month;
        var nlstr = new Date(this.year,month-1,1);
        var firstday = nlstr.getDay();
        this.firstday=firstday;
        for(var j=0 ; j<this.allday+firstday; j++){
            if(j<firstday){
                daylist[j] = ' ';
            }
            else{
                daylist[j] = j+1-firstday;
            }
        }
        this.dayList = daylist;
        //全部设置为不显示效果
        let flags = []
        for(let i =0 ;i<this.dayList.length;i++){
            flags[i]=false
        }
        //选取当天在数组中是第几个
        let todayTime = this.dayList.indexOf(day)
        //设置当天为显示效果
        flags[todayTime]=true
        this.flags = flags;
    },

    count:function (){
        var month = 10;
        month = this.month;
        var year = 10;
        year = this.year;
        var allday = 10;
        allday  = this.allday;
        if(month!=2)
        {
            if(month==4||month==6||month==9||month==11)//判断是否是相同天数的几个月，二月除外
                allday=30;
            else
                allday=31;
        }
        else
        {
            if((year%4==0&&year%100!=0)||(year%400==0))//判断是否是闰年，进行相应的改变
                allday=29;
            else
                allday=28;}
        this.allday = allday
    },

    //显示相应月份的天数
    showday: function(){
        //        for(var j=1;j<=allday;j++)
        //        {
        //            var dayElement=document.createElement("div");
        //            dayElement.innerHTML=j;
        //            dayElement.className="evrday";
        //            if(j==day)//当前日，标红
        //            dayElement.style.color="red";
        //            daterow.appendChild(dayElement);
        //        }

        var daylist = [];
        this.dayList = [];
        var day=10;
        day=this.day;
        var month = 10;
        month = this.month;
        var nlstr = new Date(this.year,month-1,1);
        var firstday = nlstr.getDay();
        this.firstday=firstday;
        //循环输出显示日历
        for(var j=0 ; j<this.allday+firstday; j++){
            //每月第一天与星期对齐
            if(j<firstday){
                daylist[j] = ' ';
            }
            else{
                daylist[j] = j+1-firstday;
                if(daylist[j]==day){//当前日，标红
                    this.$element("day").style.color="red";
                }
            }
        }
        this.dayList = daylist;
    },

    //点击上个月
    previous:function (){
        if(this.month>1)
        {
            this.month=this.month-1;
        }
        else
        {
            this.month=12;
            this.year=this.year-1;
        }
        this.year_month = this.year + "年" + this.month + "月";
        this.count();
        this.showday();
    },

    //点击下个月
    next:function (){
        if(this.month<12)
        {
            this.month=this.month+1;
        }
        else
        {
            this.month=1;
            this.year=this.year+1;
        }
        this.year_month = this.year + "年" + this.month + "月";
        this.count();
        this.showday();
    },

    selectTime:function (index){
//        let todayTime = this.dayList.indexOf(this.day)
//        this.flags[todayTime]= false
//        this.flags.splice(todayTime,1,false)
        for(let i=0;i<this.flags.length;i++){
            this.flags[i]= false;
        }
        this.flags.splice(index,1,true)
        console.log(this.flags);
        //获取选中的具体日期
        var nowday=0;//当前选中的日
        if(index<=this.firstday){//如果选择了日历开头的空白处
            this.selectedday=0;
        }else{//选择日历中的日期
            nowday=index-this.firstday+1;
            this.selectedday=this.year + "年" + this.month + "月"+ nowday + "日";
        }
        this.$element("picker").show();//选择时间
    },

    timeonchange(e) {//选择时间后点击确认按钮发生的事件
        if(this.containsecond){
            this.timevalue=e.hour+":"+e.minute+":"+e.second;
            prompt.showToast({ message:"Time:" + e.hour + ":" + e.minute + ":" + e.second })
        } else {
            this.timevalue=e.hour+":"+e.minute;
            prompt.showToast({ message:"Time:" + e.hour + ":" + e.minute })
        }
        this.year_month_day_time=this.selectedday + this.timevalue;
        this.$emit('getDate', {result:this.year_month_day_time});
    },

    timeoncancel() {
        prompt.showToast({ message:"timeoncancel" })
    },
}
