# DatePickerOH

**本项目是基于开源项目SwipeCaptcha进行鸿蒙化OpenHarmony的移植和开发的，可以通过项目标签以及github地址 （ [https://github.com/mcxtzhang/SwipeCaptcha](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fmcxtzhang%2FSwipeCaptcha) ）追踪到原安卓项目版本，该项目的讲解介绍已在社区发布，可以通过网址（ [https://harmonyos.51cto.com/posts/3402](https://gitee.com/link?target=https%3A%2F%2Fharmonyos.51cto.com%2Fposts%2F3402) ）访问相关内容。**

#### 项目介绍

- 项目名称：日期选择

- 所属系列：鸿蒙OpenHarmony的第三方组件适配移植

- 功能：实现了分别对日期、时间、日期时间进行滑动选择的效果，并将最终选择的日期时间显示出来。

- 调用差异：重构

- 开发版本：sdk8，DevEco Studio3.0 beta3

- 项目作者和维护人：李珂

- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)

- 原项目Doc地址：[https://github.com/ddd702/datePicker/](https://github.com/ddd702/datePicker/ ) 

  ![](C:\Users\Admin\Desktop\Untitled.gif)

  

####  项目介绍

- 编程语言：JavaScript
- 本项目提供了一个可供滑动选择日期和时间的组件，在选择后会将指定的日期以-分隔显示在相应的控件中，使用时需要把代码复制到所需要使用的位置即可。

#### 使用教程

1. 初始化数据，包含组件中显示的提示文字信息，以及日期、时间、和日期时间三种格式的初始值，即用户点击启动组件后界面上默认的日期和时间值。

   ```javascript
   	data: {
           selectList:["text","data","time","datetime","multitext"],
           datevalue:'点击选择日期',//组件提示文字信息
           timevalue:'点击选择时间',
           datetimevalue:'点击选择时间日期',
           containsecond:true,
           multitextselect:[1,2,0],
           datetimeselect:'2012-5-6-11-25',//默认日期时间初始值
           timeselect:'11:22:30',//默认时间初始值
           dateselect:'2021-3-2',//默认日期初始值
           textselect:'2'
       },
   ```

2. 分别设置当日期改变、时间改变、日期时间改变后触发的相应行为，并设置选择后将指定日期具体显示；通过修改相应的data值来显示到hml页面中。

```javascript
 	dateonchange(e) {//日期改变触发行为
        this.datevalue = e.year + "-" + e.month + "-" + e.day;
    },
    timeonchange(e) {//时间改变触发行为
        if(this.containsecond){
            this.timevalue=e.hour+":"+e.minute+":"+e.second;
        } else {
            this.timevalue=e.hour+":"+e.minute;
        }},
    datetimeonchange(e) {//日期时间改变触发行为
        this.datetimevalue=e.year+"-"+e.month+"-"+e.day+" "+e.hour+":"+e.minute;
    },
    popup_picker() {//显示选择的指定日期
        this.$element("picker_text").show();
    },
    selectChange(e){
        for(let i = 0;i<this.selectList.length;i++){
            if(e.newValue == this.selectList[i]){
                this.$element("picker"+i).show();
            }
        }
    },
```



#### 版本迭代

- v0.1.0-alpha

#### 版权和许可信息

- DatePicker_ohos经过[Apache License, version 2.0](https://gitee.com/link?target=http%3A%2F%2Fwww.apache.org%2Flicenses%2FLICENSE-2.0)授权许可。